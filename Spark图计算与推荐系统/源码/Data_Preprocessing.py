import random
import pandas as pd
import numpy as np


# TODO 最开始的数据预处理，为二维数据降维处理
def getResult(x: int):
    url = "C:/Users/Lenovo/Desktop/Working/A毕设-Graphx/Facebook_Data.csv"
    data = pd.read_csv(url)
    if x is not None:
        data = data.iloc[0:x, 0:x]
    print(data)

    demand_data = data.set_index("Unnamed: 0", drop=True).unstack()

    demand = demand_data.reset_index()
    demand.columns = ["host", "guest", "attitude"]
    # demand.to_csv('result_demo.csv', index=False)
    demand = demand[demand["attitude"] == 1]
    print(demand)
    if x is not None:
        demand.to_csv('result_demo_' + str(x) + '.csv', index=False)
    else:
        print("空值走的是这里------------>\n")
        demand.to_csv('result.csv', index=False)


"""
def getResultLast():
    url1 = "C:/Users/Lenovo/Desktop/Working/A毕设-Graphx/Facebook_Data.csv"
    url2 = "C:/Users/Lenovo/Desktop/Working/A毕设-Graphx/userList.csv"
    df1 = pd.read_csv(url1)
    df2 = pd.read_csv(url2)
    df1 = df1["Unnamed: 0"]
    df2["name"] = df1
    print(df2)
    df2.to_csv("userList.csv", index=False)
"""

if __name__ == '__main__':
    # TODO x=100则取前100位用户的关系图，None则代表全部关系图降维
    x = None
    getResult(x)
