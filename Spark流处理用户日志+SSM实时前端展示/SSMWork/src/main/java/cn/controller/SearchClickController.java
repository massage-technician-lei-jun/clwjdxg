package cn.controller;
import cn.pojo.Lesson;
import cn.pojo.SE;
import cn.service.CourseClickService;
import cn.service.SearchClickService;
import net.sf.json.JSONArray;
import net.sf.json.JSONObject;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * 搜索引擎Controller
 */
@Controller
@RequestMapping("/")
public class SearchClickController {

    @RequestMapping("/searchClick")
    public String toGetSearchClick(){ return "/SearchClick"; }

    /**
     *  sponseBody注解的作用是将controller的方法返回的对象通过适当的转换器转
     *  换为指定的格式之后，写入到response对象的body区，通常用来返回JSON数据或者是XML
     */
    @ResponseBody
    @RequestMapping(value = "/getSearchClickCount",method = RequestMethod.GET)
    public JSONArray searchClickCount(String data){
        JSONArray json = new JSONArray();
        ApplicationContext context = new ClassPathXmlApplicationContext(
                "beans.xml");
        SearchClickService service = (SearchClickService) context.getBean("searchService");
        SE use = (SE) context.getBean("SE");
        List<SE> ses =  service.selectSEAllSer(use);
        for(SE se : ses){
            JSONObject jo = new JSONObject();
            jo.put("name",se.getLesson());
            jo.put("count",se.getLesson_count());

            json.add(jo);
        }
        return json;
    }
}
