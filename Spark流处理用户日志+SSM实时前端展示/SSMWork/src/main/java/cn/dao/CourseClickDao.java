package cn.dao;

import cn.pojo.Lesson;

import java.util.List;

public interface CourseClickDao {
    List<Lesson> selectLessonAll(Lesson user);
}
