package cn.dao.Impl;

import cn.dao.CourseClickDao;
import cn.pojo.Lesson;
import org.mybatis.spring.SqlSessionTemplate;

import java.util.List;

public class CourseDaoImpl implements CourseClickDao{
    public SqlSessionTemplate session;
    public SqlSessionTemplate getSession(){return session;}
    public void setSession(SqlSessionTemplate session){this.session = session;}

    @Override
    public List selectLessonAll(Lesson user){
        return session.selectList("cn.mapper.LessonMapper.selectAll");
    }
}
