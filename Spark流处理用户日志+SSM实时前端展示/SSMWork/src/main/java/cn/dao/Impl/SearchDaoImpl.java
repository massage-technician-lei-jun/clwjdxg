package cn.dao.Impl;

import cn.dao.SearchClickDao;
import cn.pojo.SE;
import org.mybatis.spring.SqlSessionTemplate;

import java.util.List;

public class SearchDaoImpl implements SearchClickDao {
    public SqlSessionTemplate session;
    public SqlSessionTemplate getSession(){return session;}
    public void setSession(SqlSessionTemplate session){this.session = session;}

    @Override
    public List<SE> selectSEAll(SE user) {
        return session.selectList("cn.mapper.SEMapper.selectAll",user);
    }

}
