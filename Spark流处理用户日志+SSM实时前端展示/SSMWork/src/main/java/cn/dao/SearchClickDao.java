package cn.dao;

import cn.pojo.SE;

import java.util.List;

public interface SearchClickDao {

    List<SE> selectSEAll(SE user);
}
