package cn.mapper;

import org.apache.ibatis.annotations.Param;
import cn.pojo.Lesson;

import java.util.List;

public interface LessonMapper {
    Integer selectLessonCount(Lesson user);
    List<Lesson> selectAll();
}
