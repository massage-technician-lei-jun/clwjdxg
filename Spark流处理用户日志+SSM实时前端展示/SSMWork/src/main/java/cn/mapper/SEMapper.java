package cn.mapper;

import org.apache.ibatis.annotations.Param;
import  cn.pojo.SE;

import java.util.List;

public interface SEMapper {
    Integer selectSECount(SE user);
    List<SE> selectAll(SE user);
}
