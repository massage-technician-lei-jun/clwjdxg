package cn.pojo;

public class Lesson {
    private String lesson = null;
    private Integer lesson_count = null;

    public String getLesson() {return lesson;}

    public void setLesson(String lesson) {this.lesson = lesson;}

    public Integer getLesson_count() {return lesson_count;}

    public void setLesson_count(Integer lesson_count) {this.lesson_count = lesson_count;}

    @Override
    public String toString() {
        return "Lesson{" +
                " lesson= " + lesson +
                " ,lesson_count= " + lesson_count +
                '}';
    }
}
