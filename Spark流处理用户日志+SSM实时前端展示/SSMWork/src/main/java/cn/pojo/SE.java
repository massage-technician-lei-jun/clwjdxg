package cn.pojo;

public class SE {
    private String Se = null;
    private Integer SE_count = null;

    public String getLesson() {return Se;}

    public void setLesson(String Se) {this.Se = Se;}

    public Integer getLesson_count() {return SE_count;}

    public void setLesson_count(Integer lesson_count) {this.SE_count = lesson_count;}

    @Override
    public String toString() {
        return "Lesson{" +
                "SE=" + Se +
                "SE_count" + SE_count +
                '}';
    }
}
