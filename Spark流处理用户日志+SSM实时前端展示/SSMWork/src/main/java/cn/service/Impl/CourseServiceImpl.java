package cn.service.Impl;

import cn.pojo.Lesson;
import cn.service.CourseClickService;
import cn.dao.CourseClickDao;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("userService")
public class CourseServiceImpl implements CourseClickService{

    protected CourseClickDao dao;

    public void setDao(CourseClickDao dao) {this.dao = dao;}

    @Override
    public List<Lesson> selectLessonALlSer(Lesson user) {
        try{
            return dao.selectLessonAll(user);
        }catch (RuntimeException e){
            e.printStackTrace();
            throw e;
        }
    }
}
