package cn.service.Impl;

import cn.dao.CourseClickDao;
import cn.dao.SearchClickDao;
import cn.pojo.SE;
import cn.service.SearchClickService;

import java.util.List;

public class SearchServiceImpl implements SearchClickService {

    protected SearchClickDao dao;
    public void setDao(SearchClickDao dao) {this.dao = dao;}
    @Override
    public List<SE> selectSEAllSer(SE user) {
        try{
            return dao.selectSEAll(user);
        }catch (Exception e){
            e.printStackTrace();
            throw e;
        }
    }
}
