<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <!-- 设置每隔10秒刷新一次页面,也可以F5手动刷新-->
    <meta http-equiv="refresh" content="10">
    <script type="text/javascript" src="<c:url value="/js/echarts.js"/>"></script>
    <script src="<c:url value="/js/jquery-3.6.0.js"/>"></script>
</head>
<body>
    <div id="main" style="width: 600px;height:400px;float: left;margin-top:50px"></div>
    <div id="main2" style="width: 700px;height:400px;float: right;margin-top:50px"></div>
</body>>

<script src="<c:url value="/js/getSearchClickData.js"/>"></script>
</html>