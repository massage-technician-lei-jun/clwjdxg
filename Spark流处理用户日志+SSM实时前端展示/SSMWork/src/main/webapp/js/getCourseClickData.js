function addScript(url){
    document.write("<script language=javascript src="+echarts+"></script>");
}
var scources = [];
var scources2 = [];
var scources3 = [];
var scources4 = [];
//获得url上参数date的值
function GetQueryString(name)
{
    var reg = new RegExp("(^|&)"+ name +"=([^&]*)(&|$)");
    var r = window.location.search.substr(1).match(reg);//search,查询？后面的参数，并匹配正则
    if(r!=null)return  unescape(r[2]); return null;
}
var date = GetQueryString("date");
$.ajax({
    type:"GET",
    url:"/getCourseClickCount",
    dataType:"json",
    async:false,
    error: function (data) {
        alert("失败啦");
    },
    success:function (result) {
        if(scources.length != 0){
            scources.clean();
            scources2.clean();
            scources3.clean();
            scources4.clean();
        }
        for(var i = 0; i < result.length; i++){//饼图外侧所有数据
            scources3.push({"value":result[i].count,"name":result[i].name});

        }
        for(var i = 0; i < 5; i++){//柱状图前五数据
            scources.push(result[i].name);
            scources2.push(result[i].count);
        }
        for(var i = 0; i < 3; i++){//内测饼图前三数据
            scources4.push({"value":result[i].count,"name":result[i].name});
        }
    }
})

// 基于准备好的dom，初始化echarts实例
var myChart = echarts.init(document.getElementById('main'));

// 指定图表的配置项和数据
var option = {
    title: {
        text: '学习网实时实战课程访问量',
        subtext: '课程点击数',
        x:'center'
    },
    tooltip: {
        legend: {
            data: ['点击数']
        }
    },
    xAxis: {
        data: scources
    },
    yAxis: {},
    series: [{
        name: '点击数',
        type: 'bar',
        data: scources2
    }]
};
// 使用刚指定的配置项和数据显示图表。
myChart.setOption(option);

var myChart = echarts.init(document.getElementById('main2'));

// 指定图表的配置项和数据
var option = {
    title: {
        text: '学习网实时实战课程访问量',
        subtext: '课程点击数',
        x:'center'
    },
    tooltip: {
        trigger: 'item',
        formatter: "{a} <br/>{b}: {c} ({d}%)"
    },
    legend: {
        type: 'scroll',
        orient: 'vertical',
        right:  50,
        top: 20,
        data: scources
    },
    series: [
        {
            name: 'Access From',
            type: 'pie',
            selectedMode: 'single',
            radius: [0, '30%'],
            label: {
                position: 'inner',
                fontSize: 20
            },
            labelLine: {
                show: false
            },
            data: scources4
        },
        {
            name: 'Access From',
            type: 'pie',
            radius: ['40%', '55%'],
            labelLine: {
                length: 30
            },
            label: {
                formatter: '{a|{a}}{abg|}\n{hr|}\n  {b|{b} :}{c}  {per|{d}%}  ',
                backgroundColor: '#F6F8FC',
                borderColor: '#8C8D8E',
                borderWidth: 1,
                borderRadius: 5,
                rich: {
                    a: {
                        color: '#6E7079',
                        lineHeight: 20,
                        align: 'center'
                    },
                    hr: {
                        borderColor: '#8C8D8E',
                        width: '100%',
                        borderWidth: 1,
                        height: 0
                    },
                    b: {
                        color: '#4C5058',
                        fontSize: 15,
                        fontWeight: 'bold',
                        lineHeight: 30
                    },
                    per: {
                        color: '#fff',
                        backgroundColor: '#4C5058',
                        padding: [3, 4],
                        borderRadius: 5
                    }
                }
            },
            data: scources3
        }
    ]
};

// 使用刚指定的配置项和数据显示图表。
myChart.setOption(option);